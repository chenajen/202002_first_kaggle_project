# 202002_first_kaggle_project

      # Kaggler競賽 - Predict Future Sales

      Kaggle競賽案例學習 - [Feature engineering, xgboost](https://www.kaggle.com/dlarionov/feature-engineering-xgboost#Part-1,-perfect-features)

      ``` python
         請將Kaggle Code貼在這
         model = XGBRegressor(
         max_depth=4,
         n_estimators=1000,
         min_child_weight=300, 
         colsample_bytree=0.8, 
         subsample=0.8, 
         eta=0.3,    
         seed=42)
         
      ``` 
      ![](rank.png)
